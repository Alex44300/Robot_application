package com.iutnantes.seicom.robot;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener
{
    // Constantes pour les liens vers les "Vues"
    // et les permissions
    public static final int REQUEST_ENABLE_BT = 1;
    public static final int ACCESS_COARSE_LOCATION = 1;
    public static final int REQUEST_CONNECTION = 2;

    // Éléments de la "Vue"
    private FloatingActionButton search_button;         // Bouton en "flottant"
    private ProgressBar search_bar;                     // Animation sous forme de cercle
    private ListView device_list;                       // Liste des périphériques Bluetooth
    private ArrayAdapter<String> device_list_adapter;   // Adaptateur pour affichage
    private Switch BLE_switch;
    private static SearchActivity instance;

    // Gestion du bluetooth
    private mBluetoothEventManager bluetoothEventManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);
        instance = this;

        // Liaisons avec la "Vue"
        search_button = findViewById(R.id.floatingActionButton_search);
        search_button.setOnClickListener(this);
        search_bar = findViewById(R.id.progressBar_search);
        search_bar.setVisibility(View.INVISIBLE);
        device_list = findViewById(R.id.device_list);
        device_list_adapter = new ArrayAdapter<String>(SearchActivity.this, android.R.layout.simple_list_item_1);
        device_list.setAdapter(device_list_adapter);
        device_list.setOnItemClickListener(this);
        BLE_switch = findViewById(R.id.switch_BLE);
        BLE_switch.setOnClickListener(this);

        // On récupère le gestionnaire de connexions Bluetooth
        bluetoothEventManager = mBluetoothEventManager.getInstance();

        this.setTitle(R.string.title);

        // Vérification des permissions
        checkPermissions();
    }

    /*****METHODES APPELEES PAR ANDROID*****/

    // Création de la barre contenant le menu en haut de l'écran
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return true;
    }

    /**
     * Méthode gérant les retours d'activités
     * @param requestCode, code associé à l'activité
     * @param resultCode, echec ou réussite
     * @param data données provenant de l'activité
     */
    public void onActivityResult (int requestCode, int resultCode, Intent data)
    {
        // Si l'utilisateur ne veut pas activer le bluetooth
        // on ferme l'application
        if (requestCode == REQUEST_ENABLE_BT && resultCode != RESULT_OK)
        {
            Toast.makeText(this,R.string.bluetooth_enable, Toast.LENGTH_SHORT).show();
            unregisterReceiver(bluetoothEventManager.getBluetoothManager());
            finish();
        }
        else if (requestCode == REQUEST_ENABLE_BT && resultCode == RESULT_OK)
        {
            // Lancement de la recherche bluetooth
            bluetoothEventManager.enableBluetooth();
        }
        // Si un problème survient durant la connexion bluetooth
        // on affiche un message d'erreur
        if (requestCode == REQUEST_CONNECTION && resultCode != RESULT_OK)
        {
            Toast.makeText(this,R.string.connection_error, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Méthode gérant un appui sur le bouton "retour"
     */
    @Override
    public void onBackPressed()
    {
        // On vide le registre bluetooth
        unregisterReceiver(bluetoothEventManager.getBluetoothManager());
        // On désactive le bluetooth
        bluetoothEventManager.getmBluetoothAdapter().disable();
        super.onBackPressed();
    }

    /**
     * Méthode permettant de vérifier les permissions nécéssaires au fonctionnement de l'application
     */
    public void checkPermissions()
    {
        // Vérification des permissions (position de l'utilisateur)
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, ACCESS_COARSE_LOCATION);
        else
            // Si la permission est déjà donnée, on vérifie si le bluetooth est activé
            bluetoothEventManager.enableBluetooth();
    }

    /**
     * Méthode appelée lors de la fermeture de la boîte de dialogue demandant les permissions
     * @param requestCode, nom de la permission
     * @param permissions
     * @param grantResults, résultats, permission données ou non
     */
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        switch (requestCode)
        {
            case ACCESS_COARSE_LOCATION:
            {
                // Si la permission est données
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    // Une fois la permission donnée, on vérifie que le bluetooth est activé
                    bluetoothEventManager.enableBluetooth();
                // Si elle n'est pas donnée, on ferme l'application
                else
                {
                    unregisterReceiver(bluetoothEventManager.getBluetoothManager());
                    finish();
                }
            }
        }
    }

    /*****LISTENERS*****/

    /**
     * Méthode permettant de traiter un appui sur un item du menu
     * @param item item de menu choisi
     * @return
     */
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.menu_informations)
        {
            Intent intent = new Intent(SearchActivity.this, InformationsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        bluetoothEventManager.createConnection(i);
    }

    @Override
    public void onClick(View view)
    {
        if (view == search_button)
        {
            bluetoothEventManager.enableBluetooth();
        }
        else if(view == BLE_switch)
        {
            bluetoothEventManager.setBluetoothMode(BLE_switch.isChecked() ? true : false);
            device_list_adapter.clear();
            search_bar.setVisibility(View.INVISIBLE);
            bluetoothEventManager.resetAll();
        }
    }

    public ArrayAdapter<String> getDevice_list_adapter()
    {
        return device_list_adapter;
    }

    public ProgressBar getSearch_bar()
    {
        return search_bar;
    }

    public static SearchActivity getInstance()
    {
        return instance;
    }
}
