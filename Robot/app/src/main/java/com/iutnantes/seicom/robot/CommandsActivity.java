package com.iutnantes.seicom.robot;

import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 * Classe permettant de commander le robot
 */
public class CommandsActivity extends AppCompatActivity implements View.OnTouchListener, View.OnKeyListener
{

    private final int REQUEST_SETTINGS = 2;

    private static CommandsActivity instance;
    private TextView device_connected;
    private TextView display_UART;
    private ImageButton forward;
    private ImageButton backward;
    private ImageButton left;
    private ImageButton right;
    private ScrollView UART;
    private EditText Edit_UART;
    private int nb_messages;

    private String device_name;
    private mBluetoothEventManager bluetoothEventManager;
    private BluetoothDevice device;
    private Intent intent;

    @Override
    /**
     * Méthode permettant de construire la "Vue"
     */
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.commands_activity);
        instance = this;
        // Ajout de la "flèche" retour
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Récupération des informations provenant de la Vue précédente
        intent = getIntent();
        // Association avec le label sur la "Vue"
        device_connected = findViewById(R.id.textView_device_connected);

        // On récupère l'adresse MAC et le nom de l'appareil Bluettooth
        // passé dans les informations (Intent) de la vue précédente
        device = intent.getParcelableExtra("DEVICE");
        device_name = device.getName();
        device_connected.setText(device_name);
        bluetoothEventManager = mBluetoothEventManager.getInstance();

        // On change le titre de la vue
        this.setTitle(device_name);

        // Associations avec la vue
        // Ajouts des listeners
        forward=findViewById(R.id.imageButton_forward);
        forward.setOnTouchListener(this);

        backward=findViewById(R.id.imageButton_backward);
        backward.setOnTouchListener(this);

        left=findViewById(R.id.imageButton_left);
        left.setOnTouchListener(this);

        right=findViewById(R.id.imageButton_right);
        right.setOnTouchListener(this);

        UART=findViewById(R.id.ScrollView01);
        display_UART=findViewById(R.id.TextView_display_UART);
        Edit_UART=findViewById(R.id.editText_UART);
        Edit_UART.setOnKeyListener(this);

        // Lancement du thread de gestion de connexion
        bluetoothEventManager.setbTconnection(device);
        nb_messages = 0;
    }

    /**
     * Méthode permettant de créer le menu en haut à droite de la vue
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.commands_menu, menu);
        return true;
    }

    /**
     * Méthode permettant de traiter un appui sur la touche "retour"
     * On ferme la connexion et on revient sur la vue précédente.
     */
    @Override
    public void onBackPressed()
    {
        askEnd();
    }
    
    /**
     * Méthode permettant d'afficher les messages reçus
     * @param message le message reçu
     */
    public void display_messages(final String message)
    {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                // On ajoute le message reçu dans le champ de texte
                display_UART.append("\n" + device_name + ": " + message.toString());
                nb_messages++;

                // Au bout de 100 messages, on réinitialise le champ de texte
                if (nb_messages == 100)
                {
                    display_UART.setText(null);
                    nb_messages = 0;
                }
                UART.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });

    }

    /**
     * Permettre la création d'une boîte de dialogue demandant si l'utilisateur veut se déconnecter.
     */
    public void askEnd()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(CommandsActivity.this);
        builder.setMessage(getString(R.string.ASK_DISCONNECT) + "\"" + device_name + "\"?").setPositiveButton(R.string.YES, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                CommandsActivity.getInstance().setResult(RESULT_OK);
                bluetoothEventManager.getbTconnection().end_connection();
                CommandsActivity.getInstance().finish();
            }
        }).setNegativeButton(R.string.NO, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });
        builder.setTitle(R.string.DISCONNECT);
        AlertDialog dialog = builder.create();
        builder.show();

    }

    /**
     * Méthode permettant de traiter un appui sur un item du menu
     * @param item item de menu choisi
     * @return
     */
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_settings)
        {
            Intent intent = new Intent(CommandsActivity.this, SettingsActivity.class);
            startActivityForResult(intent, REQUEST_SETTINGS);
            return true;
        }
        if (id == R.id.menu_informations)
        {
            Intent intent = new Intent(CommandsActivity.this, InformationsActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == android.R.id.home)
        {
            bluetoothEventManager.getbTconnection().end_connection();
            this.setResult(RESULT_OK);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Méthode permettant de traiter les appuis ou relâchements d'une touche du clavier
     * @param view
     * @param i numéro de la touche
     * @param keyEvent touche enfoncée ou relâchée.
     * @return
     */
    @Override
    public boolean onKey(View view, int i, KeyEvent keyEvent)
    {
        if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER))
        {
            // Si le texte entré contient quelque chose
            if (!Edit_UART.getText().toString().isEmpty())
            {
                bluetoothEventManager.getbTconnection().setCommandToSend(Edit_UART.getText().toString());
                bluetoothEventManager.getbTconnection().setCommand("command");
                display_UART.append("\n" + Edit_UART.getText().toString());
                UART.fullScroll(ScrollView.FOCUS_DOWN);
                Edit_UART.setText("");
            }
        }
        return false;
    }

    /**
     * Méthode permettant de traiter les appuis ou relâchement sur les boutons
     * @param view
     * @param motionEvent appui ou relâchement sur un bouton
     * @return
     */
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent)
    {
        // Si on appui sur le bouton "avancer"
        if (view == forward)
        {
            // Si il est appuyé
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
            {
                // On dit au robot d'avancer
                bluetoothEventManager.getbTconnection().setCommand("forward");
                bluetoothEventManager.getbTconnection().setDirection(bluetoothEventManager.getbTconnection().DIR_FRONT);
                return true;
            }
            // Si il est relâché
            else if (motionEvent.getAction() == MotionEvent.ACTION_UP)
            {
                // On dit au robot de s'arrêter
                bluetoothEventManager.getbTconnection().setCommand("stop");
                bluetoothEventManager.getbTconnection().setDirection(bluetoothEventManager.getbTconnection().STOP);
                return true;
            }
        }
        else if (view == backward)
        {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
            {
                bluetoothEventManager.getbTconnection().setCommand("backward");
                bluetoothEventManager.getbTconnection().setDirection(bluetoothEventManager.getbTconnection().DIR_BACK);
                return true;
            }
            else if (motionEvent.getAction() == MotionEvent.ACTION_UP)
            {
                // On dit au robot de s'arrêter
                bluetoothEventManager.getbTconnection().setCommand("stop");
                bluetoothEventManager.getbTconnection().setDirection(bluetoothEventManager.getbTconnection().STOP);
                return true;
            }
        }
        else if (view == right)
        {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
            {
                bluetoothEventManager.getbTconnection().setCommand("right");
                return true;
            }
            else if (motionEvent.getAction() == MotionEvent.ACTION_UP)
            {
                bluetoothEventManager.getbTconnection().setCommand("stop");
                return true;
            }
        }
        else if (view == left)
        {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
            {
                bluetoothEventManager.getbTconnection().setCommand("left");
                return true;
            }
            else if (motionEvent.getAction() == MotionEvent.ACTION_UP)
            {
                bluetoothEventManager.getbTconnection().setCommand("stop");
                return true;
            }
        }
        return false;
    }

    /**
     * Méthode appelée lors du retour sur cette Vue
     * @param requestCode, code identifiant la vue qui a été quittée
     * @param resultCode, résultat ECHEC ou SUCCESS
     * @param data
     */
    public void onActivityResult (int requestCode, int resultCode, Intent data)
    {
        // Si nous venons de l'activité traitant les paramètres
        if (requestCode == REQUEST_SETTINGS)
            // On effectue une lecture du fichier de préférences
            // et on réactualise les commandes
            bluetoothEventManager.getbTconnection().readFile();
    }
    public static CommandsActivity getInstance()
    {
        return instance;
    }

}
