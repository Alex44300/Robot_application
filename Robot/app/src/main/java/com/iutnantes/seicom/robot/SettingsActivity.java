package com.iutnantes.seicom.robot;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;

public class SettingsActivity extends AppCompatActivity
{
    // Les 4 boutons
    private EditText edit_button_forward;   // Avant
    private EditText edit_button_left;      // Gauche
    private EditText edit_button_right;     // Droite
    private EditText edit_button_backward;  // Arrière

    private Switch switch_gyroscope;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        this.setTitle(R.string.settings);
        // Ajout de la "flèche" retour
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Associations avec la vue
        edit_button_forward = findViewById(R.id.editText_button_forward);
        edit_button_right = findViewById(R.id.editText_button_right);
        edit_button_left = findViewById(R.id.editText_button_left);
        edit_button_backward = findViewById(R.id.editText_button_backward);
        switch_gyroscope = findViewById(R.id.switch_gyroscope);

        SharedPreferences sharedPreferences;

        // On récupère le fichier de préférences (caché dans les dossiers d'Android)
        sharedPreferences = SearchActivity.getInstance().getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);

        //On regarde si l'utilisateur avait déjà enregistré des paramètres
        if(sharedPreferences.contains("FORWARD") && sharedPreferences.contains("RIGHT") &&
                sharedPreferences.contains("LEFT") && sharedPreferences.contains("BACKWARD")
                && sharedPreferences.contains("GYROSCOPE"))
        {
            edit_button_forward.setText(sharedPreferences.getString("FORWARD",null).toString());
            edit_button_right.setText(sharedPreferences.getString("RIGHT",null).toString());
            edit_button_left.setText(sharedPreferences.getString("LEFT",null).toString());
            edit_button_backward.setText(sharedPreferences.getString("BACKWARD",null).toString());
            switch_gyroscope.setChecked(sharedPreferences.getBoolean("GYROSCOPE", false));
        }
    }

    /**
     * Méthode permettant de mettre à jour les paramètres
     */
    public void update_settings()
    {
        SharedPreferences sharedPreferences;

        // On récupère le fichier de préférences (caché dans les dossiers d'Android)
        sharedPreferences = SearchActivity.getInstance().getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);

        // On enregistre les paramètres
        sharedPreferences.edit().putString("FORWARD",edit_button_forward.getText().toString()).apply();
        sharedPreferences.edit().putString("RIGHT",edit_button_right.getText().toString()).apply();
        sharedPreferences.edit().putString("LEFT",edit_button_left.getText().toString()).apply();
        sharedPreferences.edit().putString("BACKWARD",edit_button_backward.getText().toString()).apply();
        sharedPreferences.edit().putBoolean("GYROSCOPE", switch_gyroscope.isChecked()).apply();
    }
    /**
     * Méthode gérant un appui sur le bouton "retour"
     */
    @Override
    public void onBackPressed()
    {
        update_settings();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        finish();
    }

    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                update_settings();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
