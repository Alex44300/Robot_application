package com.iutnantes.seicom.robot;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.SharedPreferences;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

public class BTconnection extends Thread implements Runnable
{
    // Constantes pour la direction du robot
    public final byte DIR_FRONT = 1;
    public final byte STOP = 0;
    public final byte DIR_BACK = -1;

    // Gestion du périphérique
    private BluetoothDevice device;

    // Gestion des communications
    private mBluetoothEventManager bluetoothEventManager;

    // Gestion du thread
    private boolean stop;
    private String command;
    private String commandToSend;
    private boolean manage_gyro = false;

    // Association des commandes avec les 4 boutons
    private HashMap<String, String> commands;
    private byte direction;

    // Gestionnaire du gyroscope
    private RotationManager rotationManager;

    /**
     * Constructeur de la classe
     */
    public BTconnection(BluetoothDevice device, mBluetoothEventManager bluetoothEventManager)
    {
        this.device = device;
        this.bluetoothEventManager = bluetoothEventManager;

        stop = false;
        commandToSend = "";
        command = "";
        direction = STOP;
        commands = new HashMap<>();
        // Initialisation du gestionnaire du gyroscope
        rotationManager = new RotationManager();
        this.start();
    }


    /**
     * Méthode permettant de lire et de modifier les actions associées aux commandes
     */
    public void readFile()
    {
        SharedPreferences sharedPreferences;

        // On récupère le fichier de préférences
        sharedPreferences = SearchActivity.getInstance().getSharedPreferences("PREFS",Context.MODE_PRIVATE);

        //On regarde si l'utilisateur avait déjà enregistré des paramètres
        if (sharedPreferences.contains("FORWARD") && sharedPreferences.contains("RIGHT") &&
                sharedPreferences.contains("LEFT") && sharedPreferences.contains("BACKWARD") &&
                sharedPreferences.contains("GYROSCOPE"))
        {
            commands.put("FORWARD",sharedPreferences.getString("FORWARD",null));
            commands.put("RIGHT",sharedPreferences.getString("RIGHT",null));
            commands.put("LEFT",sharedPreferences.getString("LEFT",null));
            commands.put("BACKWARD",sharedPreferences.getString("BACKWARD",null));
            manage_gyro = sharedPreferences.getBoolean("GYROSCOPE", false);
            rotationManager.sensor_enable(manage_gyro);

        }
        // Si il n'y a aucun paramètre, on va en mettre par défaut
        else
        {
            // On ajoute des commandes par défaut
            commands.put("FORWARD","a50\n");
            commands.put("RIGHT","d100\n");
            commands.put("LEFT","g100\n");
            commands.put("BACKWARD","r50\n");

            // On les ajoute dans le fichier de préférences
            sharedPreferences.edit().putString("FORWARD","a50\n").apply();
            sharedPreferences.edit().putString("RIGHT","d100\n").apply();
            sharedPreferences.edit().putString("LEFT","g100\n").apply();
            sharedPreferences.edit().putString("BACKWARD","r50\n").apply();
            sharedPreferences.edit().putBoolean("GYROSCOPE", false).apply();
        }
    }

    /**
     * Méthode permettant de gérer les commandes à envoyer lorsque le gyroscope est activé
     */
    public void treat_gyro()
    {
        // Si le gyroscope est activé et si l'utilisateur appui sur "avant" ou arrière"
        // alors, on envoie la commande pour aller à gauche ou à droite si l'appareil est incliné à plus de 20 degrés.
        // sinon, on envoie la commande pour aller vers l'avant ou vers l'arrière.
        if ((rotationManager.getRoll() > 20) && (direction != STOP))
            send_command(commands.get("RIGHT"));
        else if ((rotationManager.getRoll()< -20) && (direction != STOP))
            send_command(commands.get("LEFT"));
        else if (direction == DIR_FRONT)
            send_command(commands.get("FORWARD"));
        else if (direction == DIR_BACK)
            send_command(commands.get("BACKWARD"));
    }

    /**
     * Méthode principale du thread
     */
    public void run()
    {
        // Paramétrage des commandes
        readFile();

        while (!stop)
        {
            // En fonction de la commande envoyée
            // Comme les commandes sont enregistrées dans un fichier et associées avec un bouton,
            // on envoie la commande correspondant au bouton
            switch(command)
            {
                case "init":
                    bluetoothEventManager.initConnection(device);
                    command = "";
                    break;

                case "forward":
                    send_command(commands.get("FORWARD"));
                    command = "";
                    break;

                case "backward":
                    send_command(commands.get("BACKWARD"));
                    command = "";
                    break;

                case "left":
                    send_command(commands.get("LEFT"));
                    command = "";
                    break;

                case "right":
                    send_command(commands.get("RIGHT"));
                    command = "";
                    break;

                case "stop":
                    send_command("s\n");
                    command = "";
                    break;

                case "command":
                    send_command(commandToSend + "\n");
                    command = "";
                    commandToSend = "";
                    break;
            }

            if (manage_gyro)
                treat_gyro();
        }
    }

    /**
     * Méthode permettant de fermer une connexion
     */
    public void end_connection()
    {
        bluetoothEventManager.endConnection();
        stop = true;
    }

    /**
     * Méthode permettant une envoyer une commande à l'appareil bluetooth
     * @param action Commande à envoyer
     */
    private void send_command(String action)
    {
        bluetoothEventManager.sendCommand(action);
    }

    /**
     * Méthode permettant de moficier l'état du thread (depuis les boutons)
     * @param __command commande à éxécuter
     */
    public void setCommand(String __command)
    {
        command =__command;
    }

    /**
     * Méthode permettant d'envoyer une commande depuis le terminal UART à l'appareil
     * @param __commandToSend Commande à envoyer à l'appareil bluetooth
     */
    public void setCommandToSend(String __commandToSend)
    {
        commandToSend =__commandToSend;
    }

    public void setDirection(byte direction)
    {
        this.direction = direction;
    }
}
