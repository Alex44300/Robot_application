package com.iutnantes.seicom.robot;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;

/**
 * Classe permettant de rediriger les demandes d'envoie, de réception, de connexion, ou de fin de connexion
 * sur le bon gestionnaire de Bluetooth (BLE ou 'classique')
 *
 * Cette API fournit les services suivants:
 * - Initialisation des différents éléments concernant le Bluetooth
 * - Création d'un connexion
 * - Initialisation d'une connexion
 * - Envoie de messages
 * - Fin d'une connexion
 * - Gestion des erreurs
 * - Gestion d'une connexion bluetooth
 */
public class mBluetoothEventManager
{
    // Constantes pour les types d'erreurs
    public static final int BLE_ERROR = 0;
    public static final int CONNECTION_ERROR = 1;

    private static mBluetoothEventManager instance;

    // Gestionnaires pour les Bluetooth 'classique' et le BLE
    private mBluetoothManager bluetoothManager;
    private mBLEManager bleManager;
    // Mode bluetooth
    private boolean bluetoothMode;

    private BluetoothAdapter mBluetoothAdapter;    // Liaison avec l'adaptateur matériel bluetooth
    private SearchActivity searchActivity;         // Vue permettant d'afficher des infos
    private ProgressDialog dialog;                 // Boîte de dialogue pour afficher les erreurs ou informations
    private BTconnection bTconnection;             // Thread gérant une connexion

    private mBluetoothEventManager()
    {
        instance = this;

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        searchActivity = SearchActivity.getInstance();

        bluetoothMode = false; // Non BLE
        bluetoothManager = new mBluetoothManager();
        bleManager = new mBLEManager();
    }

    /**
     * Méthode vérifiant si le Bluetooth est activé
     */
    public void enableBluetooth()
    {
        // Si le smartphone/tablette ne gère pas le Bluetooth, on ferme l'application.
        if (mBluetoothAdapter == null)
            searchActivity.finish();
        // Si le bluetooth n'est pas activé, on demande de l'activer.
        else if (!mBluetoothAdapter.isEnabled())
        {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            searchActivity.startActivityForResult(enableBtIntent, searchActivity.REQUEST_ENABLE_BT);
        }
        // Si il est activé, on commence la recherche de périphériques en fonction du type de bluetooth.
        else if (!bluetoothMode)
            bluetoothManager.init_bluetooth_search();
        else if (bluetoothMode)
            bleManager.init_BLE_search();
    }

    /**
     * Création d'une connexion (appairage éventuel).
     * @param deviceNumber, numéro du périphérique avec lequel il faut créer la connexion.
     */
    public void createConnection(int deviceNumber)
    {
        if (!bluetoothMode)
            bluetoothManager.createBluetoothConnection(deviceNumber);
        else
            bleManager.createBLEConnection(deviceNumber);
    }

    /**
     * Initialisation d'une connexion (récupération des UUID (identifiants)).
     * En mode BLE, cette méthode retourne 'vrai' car les UUID sont gérés d'une manière différente (serveur GATT, interruptions...).
     * @param device, périphérique Bluetooth.
     * @return 'true' si la connexion est réussie, 'false' autrement.
     */
    public boolean initConnection(BluetoothDevice device)
    {
        if (!bluetoothMode)
            return bluetoothManager.initConnection(device);

        return true;
    }

    /**
     * Fermeture d'une connexion Bluetooth.
     */
    public void endConnection()
    {
        if (!bluetoothMode)
            bluetoothManager.endBluetoothConnection();
        else
            bleManager.endBLEConnection();
    }

    /**
     * Envoie d'un message au périphérique.
     * @param command, message à envoyer.
     */
    public  void sendCommand(String command)
    {
        if (!bluetoothMode)
            bluetoothManager.sendBluetoothCommand(command);
        else
            bleManager.sendCommand(command);
    }

    /**
     * Permettre d'afficher une boîte de dialogue informant l'utilisateur d'une erreur.
     * @param error_type, erreur de connexion, non support du BLE...
     */
    public void show_error_dialog(int error_type)
    {
        switch (error_type)
        {
            case BLE_ERROR:
                searchActivity.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(searchActivity);
                        builder.setMessage(R.string.BLE_NOT_SUPPORTED).setPositiveButton("OK", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                dialog.dismiss();
                            }
                        });
                        builder.setTitle(R.string.BLE_ERROR);
                        AlertDialog dialog = builder.create();
                        builder.show();
                    }
                });
                break;

            case CONNECTION_ERROR:
                searchActivity.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(searchActivity);
                        builder.setMessage(R.string.CONNECTION_NOT_POSSIBLE).setPositiveButton("OK", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                dialog.dismiss();
                            }
                        });
                        builder.setTitle(R.string.CONNECTION_ERROR);
                        AlertDialog dialog = builder.create();
                        builder.show();
                    }
                });
                break;
        }
    }

    /**
     * Permettre de modifier le mode de Bluetooth.
     * @param mode, true, BLE, false, Bluetooth 'classique'.
     */
    public void setBluetoothMode(boolean mode)
    {
        bluetoothMode = mode;
    }

    /**
     * Permettre de réinitialiser les listes de périphériques et l'affichage de cette dernière.
     */
    public void resetAll()
    {
        searchActivity.getDevice_list_adapter().clear();
        bluetoothManager.resetAll();
        bleManager.resetAll();
    }

    /**
     * Permettre d'afficher un dialogue informant que la connexion est en cours.
     * @param show, 'true' pour afficher, 'false', pour ne pas l'afficher.
     */
    public void showDialog(final boolean show)
    {
        searchActivity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                if (show)
                    dialog = ProgressDialog.show(searchActivity, searchActivity.getResources().getString(R.string.CONNECTION), searchActivity.getResources().getString(R.string.CONNECTION_WAIT));
                else
                    dialog.dismiss();
            }
        });
    }


    /**
     * Récupérer l'adaptateur bluetooth.
     * @return
     */
    public BluetoothAdapter getmBluetoothAdapter()
    {
        return mBluetoothAdapter;
    }

    /**
     * Permettre de retourner la 'Vue' principale.
     * @return
     */
    public SearchActivity getSearchActivity()
    {
        return searchActivity;
    }

    /**
     * Permettre de retourner le gestionnaire BLE.
     * @return
     */
    public mBLEManager getBleManager()
    {
        return bleManager;
    }

    /**
     * Permettre de retourner le gestionnaire de Bluetooth 'classique'.
     * @return
     */
    public mBluetoothManager getBluetoothManager()
    {
        return bluetoothManager;
    }

    /**
     * Permettre de retourner le thread de connexion.
     * @return
     */
    public BTconnection getbTconnection()
    {
        return bTconnection;
    }

    /**
     * Permettre de démarrer le thread de connexion.
     * @param device, périphérique avec lequel on communique.
     */
    public void setbTconnection(BluetoothDevice device)
    {
        bTconnection = new BTconnection(device, this);
    }

    /**
     * Permettre de retourner l'instance de cette classe.
     * @return
     */
    public static mBluetoothEventManager getInstance()
    {
        if (instance == null)
            return new mBluetoothEventManager();
        else
            return instance;
    }
}
