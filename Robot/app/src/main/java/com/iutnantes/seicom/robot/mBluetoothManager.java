package com.iutnantes.seicom.robot;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.Vector;

/**
 * Classe permettant de gérer un connexion Bluetooth 'classique'.
 */
public class mBluetoothManager extends BroadcastReceiver
{
    // Activité recherche
    private SearchActivity searchActivity;
    // Gestion du bluetooth
    private BluetoothAdapter mBluetoothAdapter;         // Gestion du blutooth
    private mBluetoothEventManager bluetoothEventManager;
    private IntentFilter filter;                        // Filtres sur les "interruptions"

    // Permettre d'enregistrer les appareils bluetooth
    private Vector<BluetoothDevice> devices;
    private String device_uuid;
    private BluetoothSocket socket;
    private BTreception reception;

    //Timer pour arrêter la recherche de périphériques bluetooth
    private Timer search_timer;
    private TimerTask interrupt;

    public mBluetoothManager()
    {
        this.bluetoothEventManager = mBluetoothEventManager.getInstance();
        this.searchActivity = bluetoothEventManager.getSearchActivity();
        this. mBluetoothAdapter = bluetoothEventManager.getmBluetoothAdapter();

        devices = new Vector<>();
        filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);          // Filtre sur recherche de périphériques
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);     // Filtre sur changement d'état de l'appairage
        searchActivity.registerReceiver(this, filter);
    }

    /*** INTERRUPTION SUR LE BLUETOOTH ***/

    @Override
    public void onReceive(Context context, Intent intent)
    {
        int signal_strenght;
        // On récupère les résultats de la recherche
        String action=intent.getAction();

        if (BluetoothDevice.ACTION_FOUND.equals(action))
        {
            // On récupère l'appareil détecté
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            // Récupération force signal
            signal_strenght = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,Short.MIN_VALUE);
            // Enregistrement de l'appareil si il possède un nom
            if (device.getName() != null)
            {
                if (!device.getName().isEmpty())
                {
                    searchActivity.getDevice_list_adapter().add(device.getName() + " ( " + Integer.toString(signal_strenght) +" dBm )");
                    devices.add(device);
                }
            }
        }

        // Si l'état de l'appairage change
        if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action))
        {
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            // Si l'appareil est appairé, on se connecte
            if(device.getBondState() == BluetoothDevice.BOND_BONDED)
                setConnection(device);
        }
    }


    /**
     * Méthode permettant de rechercher des périphériques bluetooth
     * Fin de la recherche au bout de 10 secondes
     */
    public void init_bluetooth_search()
    {
        // On efface la liste des appareils déjà trouvés
        searchActivity.getDevice_list_adapter().clear();
        devices.clear();
        search_bluetooth_devices();
    }

    /**
     * Permettre de rechercher des périphériques Bluetooth.
     */
    private void search_bluetooth_devices()
    {
        mBluetoothAdapter.startDiscovery();
        searchActivity.getSearch_bar().setVisibility(View.VISIBLE);
        devices.clear();

        // Nouvelle tâche d'interruption
        interrupt = new TimerTask()
        {
            @Override
            public void run()
            {
                // Arrêt du timer
                search_timer.cancel();
                // Fin de la recherche
                mBluetoothAdapter.cancelDiscovery();
                // Mise à jour de la "Vue"
                searchActivity.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        // On rend invisible l'animation de recherche
                        searchActivity.getSearch_bar().setVisibility(View.INVISIBLE);
                        // Si aucun périphérique n'est trouvé, on l'indique à l'utilisateur
                        if (devices.isEmpty())
                            Toast.makeText(searchActivity.getApplicationContext(), R.string.peripherals, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };

        // Instanciation du timer
        search_timer = new Timer();
        // Programmation de l'IT
        search_timer.schedule(interrupt, 10000);
    }

    /**
     * Permettre de créer une connexion (démarrer le thread gérant la connexion ou l'appairage si
     * il n'est pas effectué).
     * @param device_num, numéro du périphérique auquel on veut se connecter.
     */
    public void createBluetoothConnection(int device_num)
    {
        // Si l'appareil est déjà appairé, on s'y connecte
        if (devices.get(device_num).getBondState() == BluetoothDevice.BOND_BONDED)
           setConnection(devices.get(device_num));
            // Sinon, on demande à réaliser l'appairage et on s'y connectera ensuite
        else
            devices.get(device_num).createBond();
    }

    /**
     * Permettre de démarrer le thread de connexion.
     * @param device, périphérique avec lequel on veut communiquer.
     */
    public void setConnection(BluetoothDevice device)
    {
        bluetoothEventManager.setbTconnection(device);
        bluetoothEventManager.getbTconnection().setCommand("init");
    }

    /**
     * Permettre d'afficher la 'Vue' contenant les commandes.
     * @param device, périphérique auquel on est connecté.
     */
    public void connectDevice(BluetoothDevice device)
    {
        // Si l'appareil est déjà appairé, on s'y connecte
        Intent intent = new Intent(searchActivity, CommandsActivity.class);
        intent.putExtra("DEVICE",device);
        bluetoothEventManager.showDialog(false);
        searchActivity.startActivityForResult(intent, searchActivity.REQUEST_CONNECTION);
    }

    /**
     * Permettre d'initialiser une connexion (sockets, UUID...)
     * @param device, périphérique avec lequel la connexion doit être initialisée.
     * @return
     */
    public boolean initConnection(BluetoothDevice device)
    {
        // Affichager boîte de dialogue info connexion...
        bluetoothEventManager.showDialog(true);

        try
        {
            mBluetoothAdapter.cancelDiscovery();
            device_uuid = device.getUuids()[0].toString();
            mBluetoothAdapter.getRemoteDevice(device.getAddress());
            // On tente de se connecter
            socket = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString(device_uuid));
            socket.connect();
            reception = new BTreception(socket);
            connectDevice(device);
            return true;
        }
        catch (IOException e)
        {
            Log.e("[CONNECTION ERROR]", "The device: " + device.getName() + " IO Exception");

        }
        catch (NullPointerException e)
        {
            Log.e("[CONNECTION ERROR]", "The device: " + device.getName() + " does not have UUID !");
            e.printStackTrace();
        }

        bluetoothEventManager.showDialog(false);
        bluetoothEventManager.show_error_dialog(mBluetoothEventManager.CONNECTION_ERROR);
        return false;
    }

    /**
     * Permettre d'envoyer une commande.
     * @param command, commande à envoyer.
     * @return
     */
    public boolean sendBluetoothCommand(String command)
    {
        try
        {
            socket.getOutputStream().write(command.getBytes());
            socket.getOutputStream().flush();

            return true;
        }
        // En cas de problème, on ferme tout
        catch (IOException e)
        {
            CommandsActivity.getInstance().finish();
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Permettre de fermer une connexion.
     * @return
     */
    public boolean endBluetoothConnection()
    {
        try
        {
            // Fin de la réception
            reception.interrupt();
            // Fermeture de la connexion
            socket.close();
            CommandsActivity.getInstance().finish();

            return true;
        }
        // En cas de problème
        catch (IOException e)
        {
            CommandsActivity.getInstance().finish();
            e.printStackTrace();
        }
        catch (NullPointerException e)
        {
            CommandsActivity.getInstance().finish();
        }

        return false;
    }

    /**
     * Permettre de réinitialiser le timer ainsi que la recherche de périphériques.
     */
    public void resetAll()
    {
        search_timer.cancel();
        mBluetoothAdapter.cancelDiscovery();
    }
}
