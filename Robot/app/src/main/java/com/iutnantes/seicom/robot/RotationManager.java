package com.iutnantes.seicom.robot;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class RotationManager implements SensorEventListener
{
    // Constantes
    public final int roll_Offset = 90;

    // Gestionnaire des capteurs
    private SensorManager mSensorManager;

    // Valeurs des mesures
    private double azimuth, pitch, roll;
    // Permettre l'activation ou non des mesures
    private boolean rotation_enable;

    // Tableaux pour enregistrer les mesures
    private float gravity[];
    private float magnetic[];

    private float accels[];
    private float mags[];
    private float values[];
    float outGravity[];

    /***
     * Constructeur de la classe
     */
    public RotationManager()
    {
        // Instanciation du gestionnaire des capteurs
        mSensorManager = (SensorManager) SearchActivity.getInstance().getApplicationContext().getSystemService(Context.SENSOR_SERVICE);
        // Ajout des interruptions pour la lecture des capteurs (accéléromètre et magnétique), toutes les 10 ms
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), 10000);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), 10000);

        accels = new float[3];
        mags = new float[3];
        values = new float[3];
        azimuth = pitch = roll = 0;
        rotation_enable = false;
    }

    /**
     * Traitement des évènements
     * @param event
     */
    @Override
    public void onSensorChanged(SensorEvent event)
    {
        if (rotation_enable)
        {
            // On sauvegarde les valeurs dans les tableaux
            switch (event.sensor.getType())
            {
                case Sensor.TYPE_MAGNETIC_FIELD:
                    mags = event.values.clone();
                    break;

                case Sensor.TYPE_ACCELEROMETER:
                    accels = event.values.clone();
                    break;

            }

            // Si on a bien des valeurs, on les traite
            if (mags != null && accels != null)
            {
                gravity = new float[9];
                magnetic = new float[9];

                // Matrice des valeurs
                SensorManager.getRotationMatrix(gravity, magnetic, accels, mags);
                outGravity = new float[9];

                // Calculs
                SensorManager.remapCoordinateSystem(gravity, SensorManager.AXIS_X, SensorManager.AXIS_Z, outGravity);
                SensorManager.getOrientation(outGravity, values);

                // Conversions en degrés
                azimuth = Math.toDegrees(values[0]);
                pitch = Math.toDegrees(values[1]);
                roll = Math.toDegrees(values[2]);
                mags = null;
                accels = null;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }

    /**
     * Méthode permettant d'activer ou non les mesures
     * @param enable
     */
    public void sensor_enable(boolean enable)
    {
        rotation_enable = enable;
    }

    /*** ACCESSEURS ***/

    public int getAzimuth()
    {
        return (int)azimuth;
    }

    public int getPitch()
    {
        return (int)pitch;
    }

    public int getRoll()
    {
        // Lorsque l'appareil est tenu comme un volant, l'angle est de 90° initialement.
        // Ainsi, si on veut avoir un angle compris entre - 90 et 0° (inclinaison
        // vers la gauche) et 0 et 90 (inclinaison vers la droite), un offset est nécéssaire
        return (int)roll + roll_Offset;
    }

    public boolean isRotation_enabled()
    {
        return rotation_enable;
    }
}
