package com.iutnantes.seicom.robot;

import android.bluetooth.BluetoothSocket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Classe permettant de lire les données provenant du Bluetooth (non BLE)
 */
public class BTreception extends Thread implements Runnable
{
    private BluetoothSocket socket; // Socket sur laquelle on est connecté
    private BufferedReader data;    // Données reçues

    /**
     * Constructeur de la classe
     * @param __socket, socket sur laquelle la réception doit se faire
     */
    public BTreception(BluetoothSocket __socket)
    {
        socket = __socket;
        try
        {
            // On initialise le buffer de données
            data = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            // Lancement du thread
            this.start();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
    public void run()
    {
            while(!this.isInterrupted())
                // On regarde si des données sont présentes dans le buffer
                reception();
    }

    /**
     * Méthode permettant de recevoir les données et de les afficher dans le terminal UART
     */
    public void reception()
    {
        try
        {
            // On attend des données
            CommandsActivity.getInstance().display_messages(data.readLine());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
