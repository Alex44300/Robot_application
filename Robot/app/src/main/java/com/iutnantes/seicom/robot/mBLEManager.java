package com.iutnantes.seicom.robot;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import java.nio.charset.Charset;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

/**
 * Classe permettant de gérer une connexion avec un périphérique utilisant le Bluetooth Low Energy (BLE)
 *
 * Fonctionnement du Bluetooth Low Energy:
 * - Chaque périphérique BLE possède plusieurs services (capteurs, envoie de données UART, etc).
 * - Chaque service est composé de plusieurs caractéristiques (exemple, pour le service UART, on peut lire et écrire des données, soit 2 services).
 * - Une interruption est possible sur chacune des caractéristiques (sur écriture, lecture ou changement d'état).
 *
 * Cette API implémente les fonctionnalités suivantes:
 *  - Recherche de périphériques compatibles BLE.
 *  - Connexion à un périphérique.
 *  - Envoie de messages à ce périphérique.
 *  - Réception de messages (sous interruption).
 */
public class mBLEManager extends BluetoothGattCallback
{
    // Constantes pour les tableaux (voir doc ADAFRUIT : https://learn.adafruit.com/getting-started-with-the-nrf8001-bluefruit-le-breakout/adding-app-support).
    private static final int UART_SERVICE = 4;
    private static final int RX_CHARACTERISTIC = 0;
    private static final int CLIENT_CHARACTERISTIC = 1;
    private static final int TX_CHARACTERISTIC = 1;

    // Activité recherche.
    private SearchActivity searchActivity;

    // Gestion du bluetooth.
    private BluetoothAdapter mBluetoothAdapter;
    private mBluetoothEventManager bluetoothEventManager;   // Gestionnaire d'évèments.
    private BluetoothLeScanner mBluetoothLeScanner;         // Scanner sur les bluetooth (détection).
    private ScanCallback mScanCallback;                     // Interruption si détection de pérphériques.
    private BluetoothGatt mBluetoothGatt;                   // Gestionnaire d'interruptions BLE.
    private BluetoothGattCharacteristic tx;                 // Transmission.
    private BluetoothGattCharacteristic rx;                 // Réception.
    private StringBuilder receivedData;                     // Mise en forme des données reçues.

    // Permettre d'enregistrer les périphériques BLE.
    private Vector<BluetoothDevice> BLE_devices;

    //Timer pour arrêter la recherche de périphériques BLE.
    private Timer search_timer;

    public mBLEManager()
    {
        super();

        // Initialisations.
        this.bluetoothEventManager = mBluetoothEventManager.getInstance();
        this.mBluetoothAdapter = bluetoothEventManager.getmBluetoothAdapter();
        this.searchActivity = bluetoothEventManager.getSearchActivity();

        BLE_devices = new Vector<>();
        receivedData = new StringBuilder();

        // Interruption lors de la détection d'un périphérique BLE.
        mScanCallback = new ScanCallback()
        {
            @Override
            public void onScanResult(int callbackType, ScanResult result)
            {
                int signal_strenght;
                boolean should_add = true;

                super.onScanResult(callbackType, result);

                // Récupération force signal.
                signal_strenght = result.getRssi();

                // Comme en mode BLE, on peut trouver plusieurs fois le même périphérique, on ne l'ajoutera pas à chaque fois dans la liste
                // (comparaison avec les adresses MAC).
                for (int i = 0 ; i< BLE_devices.size(); i++)
                {
                    if (result.getDevice().getAddress().equals(BLE_devices.get(i).getAddress()))
                            should_add = false;
                    else if (result.getDevice().getName() == null)
                            should_add = false;
                    else if (result.getDevice().getName().isEmpty())
                            should_add = false;
                }
                // Affichage et mise à jour des listes.
                if (should_add)
                {
                    searchActivity.getDevice_list_adapter().add(result.getDevice().getName()+ " ( " + Integer.toString(signal_strenght)+" dBm )");
                    BLE_devices.add(result.getDevice());
                }
            }
        };
    }

    /**
     * Interruption sur changement d'état de la connexion.
     * @param gatt, serveur gérant les interruptions.
     * @param status, état de la connexion.
     * @param newState, notification changement d'état.
     */
    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState)
    {
        super.onConnectionStateChange(gatt, status, newState);

        if (newState == BluetoothGatt.STATE_CONNECTED)
        {
            if (status == BluetoothGatt.GATT_SUCCESS)
            {
                mBluetoothGatt.discoverServices();
            }
            else if (status == BluetoothGatt.GATT_FAILURE)
            {
                bluetoothEventManager.show_error_dialog(mBluetoothEventManager.CONNECTION_ERROR);
            }
        }
        bluetoothEventManager.showDialog(false);
    }

    /**
     * Interruption sur découvertes de services
     * ( En BLE, chaque appareil possède plusieurs services (capteurs, transmissions de données...)
     * @param gatt, serveur BLE.
     * @param status, état.
     */
    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status)
    {
        super.onServicesDiscovered(gatt, status);

        if (status == BluetoothGatt.GATT_SUCCESS)
        {
            // Association des services avec leur UUID (voir documentations constructeur)
            // Exemple avec Adafruit : https://learn.adafruit.com/getting-started-with-the-nrf8001-bluefruit-le-breakout/adding-app-support
            rx = gatt.getService(gatt.getServices().get(UART_SERVICE).getUuid()).getCharacteristics().get(RX_CHARACTERISTIC);
            tx = gatt.getService(gatt.getServices().get(UART_SERVICE).getUuid()).getCharacteristics().get(TX_CHARACTERISTIC);

            // Activation des interruptions sur réception de données.
            gatt.setCharacteristicNotification(rx, true);

            // Permettre l'activation de l'interruption.
            BluetoothGattDescriptor descriptor = rx.getDescriptors().get(CLIENT_CHARACTERISTIC);
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            gatt.writeDescriptor(descriptor);

            // Changement de vue (toutes les initialisations sont terminées).
            Intent intent = new Intent(searchActivity, CommandsActivity.class);
            intent.putExtra("DEVICE", gatt.getDevice());
            bluetoothEventManager.showDialog(false);
            searchActivity.startActivityForResult(intent, SearchActivity.REQUEST_CONNECTION);
        }
        else
            Log.e("[GATT SERVICES]", "Erreur lors de l'établissement de la connexion");
    }

    /**
     * Interruption sur changement d'une caractéristique (réception de données).
     * @param gatt
     * @param characteristic
     */
    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic)
    {
        super.onCharacteristicChanged(gatt, characteristic);

        // On construit la chaîne de catactères tant qu'elle n'est pas terminée, puis, on l'affiche.
        receivedData.append(characteristic.getStringValue(0));
        if (receivedData.substring(receivedData.length() -1).contains("\n"))
        {
            CommandsActivity.getInstance().display_messages(receivedData.toString());
            receivedData = new StringBuilder();
        }
    }

    /**
     * Interruption sur écriture d'une caractéristique (envoie de données).
     * @param gatt
     * @param characteristic
     * @param status
     */
    @Override
    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status)
    {
        super.onCharacteristicWrite(gatt, characteristic, status);

        if (status == BluetoothGatt.GATT_SUCCESS)
            Log.i("[GATT WRITE]", "OK");
    }

    /**
     * Initialisation de la recherche de périphériques (si le smartphone/tablette est compatible avec le Bluetooth Low Energy).
     */
    public void init_BLE_search()
    {
        BluetoothManager mBluetoothManager;
        if (enable_BLE())
        {
            mBluetoothManager = (BluetoothManager) searchActivity.getSystemService(SearchActivity.BLUETOOTH_SERVICE);
            try
            {
                mBluetoothAdapter = mBluetoothManager.getAdapter();
                search_BLE_devices();
            }
            catch (NullPointerException e)
            {
                e.printStackTrace();
                bluetoothEventManager.show_error_dialog(mBluetoothEventManager.BLE_ERROR);
                Log.e("[ERREUR ADAPTEUR]", "adapteur bluetooth null");
            }
        }
        else
            bluetoothEventManager.show_error_dialog(mBluetoothEventManager.BLE_ERROR);
    }

    /**
     * Lancement de la recherche de périphériques BLE.
     */
    private void search_BLE_devices()
    {
        // Lancement de la recherche bluetooth.
        mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        mBluetoothLeScanner.startScan(mScanCallback);
        searchActivity.getSearch_bar().setVisibility(View.VISIBLE);
        BLE_devices.clear();
        searchActivity.getDevice_list_adapter().clear();

        // Nouvelle tâche d'interruption.
        TimerTask interrupt = new TimerTask() {
            @Override
            public void run() {
                // Arrêt du timer.
                search_timer.cancel();
                // Fin de la recherche.
                mBluetoothLeScanner.stopScan(mScanCallback);
                // Mise à jour de la "Vue".
                searchActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // On rend invisible l'animation de recherche.
                        searchActivity.getSearch_bar().setVisibility(View.INVISIBLE);
                        // Si aucun périphérique n'est trouvé, on l'indique à l'utilisateur.
                        if (BLE_devices.isEmpty())
                            Toast.makeText(searchActivity.getApplicationContext(), R.string.peripherals, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };

        // Instanciation du timer.
        search_timer = new Timer();
        // Programmation de l'IT.
        search_timer.schedule(interrupt, 10000);
    }

    /**
     * Création d'un connexion.
     * @param device_num, numéro du périphérique auquel on souhaite se connecter.
     */
    public void createBLEConnection(int device_num)
    {
        connectBLEDevice(BLE_devices.get(device_num));
    }

    /**
     * Connexion au périphérique BLE.
     * @param device, périphérique sur lequel on veut se connecter.
     * @return
     */
    public boolean connectBLEDevice(BluetoothDevice device)
    {
        bluetoothEventManager.showDialog(true);
        // Connexion automatique, interruptions gérées par cette classe.
        mBluetoothGatt = device.connectGatt(searchActivity, true, this);

        return true;
    }

    /**
     * Fin d'une connexion.
     */
    public void endBLEConnection()
    {
        mBluetoothGatt.disconnect();
        mBluetoothGatt.close();
    }

    /**
     * Permettre de savoir si le smartphone/tablette est compatible avec le BLE.
     */
    private boolean enable_BLE()
    {
        return searchActivity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
    }

    /**
     * Permettre d'envoyer une commande au périphérique BLE.
     * @param data, données à envoyer au périphérique.
     */
    public void sendCommand(String data)
    {
        tx.setValue(data.getBytes(Charset.forName("UTF-8")));
        mBluetoothGatt.writeCharacteristic(tx);
    }

    /**
     * Permettre de réinitialiser les listes de périphériques BLE et d'arrêter le scan.
     */
    public void resetAll()
    {
        try
        {
            BLE_devices.clear();
            search_timer.cancel();
            mBluetoothLeScanner.stopScan(mScanCallback);
        }
        catch (NullPointerException e)
        {
            Log.e("[RESET ALL]", "BLE non utilisé !");
        }
    }
}
