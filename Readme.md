<h1> Robot application </h1>

<p> Cette application permet actuellement de communiquer avec une carte Arduino et avec un périphérique Adafruit grâce à la liaison Bluetooth.</p>

<h2> Fonctionnalités actuelles </h2>
<ul>
	<li> Recherche des périphériques Bluetooth </li>
	<li> Connexion à un périphérique (appairage automatique) </li>
	<li> Contrôle avec un terminal UART ou sous forme d'une manette de jeu </li>
	<li> Possibilité de diriger une base mobile avec le gyroscope </li>
	<li> Possibilité de remapper les touches de la manette (appui uniquement) </li>
	<li> Prise en charge des périphériques BLE </li>
</ul>

<h2> Fonctionnalités futures </h2>
<ul>
	<li> Ajout configuration boutons sur relâchement </li>
</ul>

<h2> Architecture de l'application </h2>
<h3> Les différentes 'Vues'</h3>
<p> Le programme de l'application comporte plusieurs 'Vues' permettant d'afficher les informations pour l'utilisateur. Ces 'Vues' concernent : </p>
<ul>
	<li> La recherche de périphériques bluetooth</li>
	<li> Les commandes du périphérique </li>
	<li> Le paramétrage des boutons </li>
	<li> Les informations sur l'application </li>
</ul>
<h3> Les différents gestionnaires pour le Bluetooth</h3>
<p> Le bluetooth est géré un travers divers gestionnaires : </p>
<ul>
	<li> Un gestionnaire global qui permet de rediriger les informations provenant des gestionnaires de bluetooth 'classique' ou du 'BLE' vers les 'Vues' et inversement</li>
	<li> Un gestionnaire de bluetooth 'classique' qui permet de communiquer avec les péripéhriques bluetooth.</li>
	<li> Un gestionnaire BLE qui permet de communiquer avec les périphériques compatibles BLE uniquement </li>
</ul>

<h2> Installation </h2>

<p>L'application est prévue pour fonctionner sur les smartphones et tablettes équipées
de la version 6.0 d'Android au minimum.</p>

<h2> Téléchargement </h2>

<p>La dernière version de l'application sera disponible dans le dossier "Release".</p>
